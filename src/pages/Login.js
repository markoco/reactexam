import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { ErrorToast } from '../components/Toasts';

const Login = () => {
    const history = useHistory();

    const[email,setEmail] = useState('');
    const[password,setPassword] = useState('');

    useEffect(()=>{
        sessionStorage.clear();
    },[])

    const handleEmailOnChange = e => {
        setEmail(e.target.value)
    }

    const handlePasswordOnChange = e => {
        setPassword(e.target.value)
    }

    const handleLogin = () =>{
        if(email !=='' && password!==''){
            history.push('/Home');
            sessionStorage.email = email;
        }else{
            ErrorToast("Plese fill out the form properly!")
        }
        
    }

    return (  
        <div className="vh-100 vw-100 bg-light d-flex align-items-center justify-content-center">
            <div className="col-lg-4">
                <div className="form-group">
                    <input 
                        type="email"
                        className="form-control" 
                        placeholder="E-mail"
                        onChange={handleEmailOnChange}
                    />
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        className="form-control" 
                        placeholder="Password"
                        onChange={handlePasswordOnChange}
                    />
                </div>
                <button 
                    className="btn btn-info"
                    onClick={handleLogin}
                >
                    Login
                </button>
            </div>
        </div>
    );
}
 
export default Login;