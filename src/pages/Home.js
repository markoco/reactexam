import React, { useState,useEffect } from 'react';
import Cards from '../components/Cards';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import PostForm from '../components/PostForm';
import { AddedToast, DeleteToast } from '../components/Toasts';

const Home = () => {
    
    const[showForm,setShowForm] = useState(false);
    const [posts, setPosts] = useState([]);


    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/posts')
          .then(res => res.json())
          .then(res => {
            setPosts(res);
          });
    }, []);

    const handleDeletePost = (id) => {
        let oldPosts = [...posts]

        let newPosts = oldPosts.filter(post => {
            return post.id !== id
        });

        setPosts(newPosts);
        DeleteToast(id)
    }

    const handleSavePost = (title,body) => {
        const id = posts.length+1;
        let newPosts = [];
        let newPost = {
            id,
            userId: 1,
            title,
            body,
            topicId: 1
          }
    
          newPosts = [newPost, ...posts];
          setPosts(newPosts);
          setShowForm(false);

          AddedToast(title);
    }


    return (  
        <div className="position-relative">
            <Header/>
           
            <div className="row mx-0 px-0">
                <Sidebar/>
                <Cards 
                    posts={posts}
                    handleDeletePost={handleDeletePost}
                />
            </div>

            <div className="vh-100 vw-100 position-fixed">
                <button 
                    onClick={()=>setShowForm(true)}
                    className="btn btn-success position-fixed" 
                    style={{
                        bottom:"15px",
                        right:"15px", 
                        zIndex:2
                }}>
                    Add Post +
                </button>
            </div>
            <PostForm 
                showForm={showForm}
                handleSavePost={handleSavePost} 
                toggleForm={()=>setShowForm(false)}
            />
            
        </div>
    );
}
 
export default Home;