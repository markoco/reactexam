import React from 'react';
import {
  BrowserRouter, 
  Route, 
  Switch
} from 'react-router-dom';

const App = () =>{

  const Login = React.lazy(() => import('./pages/Login'));
  const Home = React.lazy(() => import('./pages/Home'));
    
  return(
    <>
      <BrowserRouter>
        <React.Suspense
            fallback={""}
        >
            <Switch>
                <Route
                    path="/"
                    exact
                    render={props => <Login{...props} />}
                />
                <Route
                    path="/Home"
                    render={props => 
                      !sessionStorage.email
                      ?<Login{...props} /> 
                      :<Home{...props} />
                    }
                />
            </Switch>
        </React.Suspense>
      </BrowserRouter>
    </>
  )
  
}

export default App;
