import React from 'react';

const Card = ({post,handleDeletePost}) => {
    return (  
        <div 
            className="card m-3" 
            style={{
                width:"25rem", 
                minHeight:"300px"
            }}
        >
            <div 
                className="card-header bg-white" 
                style={{  
                    overflow: "hidden",
                    whiteSpace:"nowrap",
                    textOverflow: "ellipsis"
                }}>
                {post.title}
            </div>
            <div className="card-body">{post.body}</div>
            <div className="card-footer text-right">
                <button 
                    className="btn btn-danger m-1"
                    onClick={()=>handleDeletePost(post.id)}
                >
                    Delete
                </button>
            </div>
        </div>
    );
}
 
export default Card;