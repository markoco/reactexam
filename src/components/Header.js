import React from 'react';
import { Button } from 'reactstrap';
import { useHistory } from 'react-router-dom';
const Header = () => {
    const history = useHistory();

    const handleLogout = () =>{
        history.push('/');
    }

    return (  
        <div 
            className="bg-light text-right"
            style={{
                height:"75px", 
                position:"sticky", 
                top: 0, 
                zIndex:1, 
                borderBottom:"1px solid #dae0e3"
            }}
        >
            <Button 
                className="m-3 text-light" 
                color="warning"
                onClick={handleLogout}
            >
                Logout
            </Button>
        </div>
    );
}
 
export default Header;
