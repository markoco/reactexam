import React from 'react';
import Card from './Card';

const Cards = ({posts,handleDeletePost}) => {
   
    return (  
        <div className="col-lg-9 d-flex flex-wrap justify-content-center align-items-center">
            {posts.map((post,index)=>(
                <Card 
                    key={index} 
                    post={post}
                    handleDeletePost={handleDeletePost}
                />
            ))}
        </div>
    );
}
 
export default Cards;