import React, { useState } from 'react';
import { Modal, ModalBody, Input, FormGroup, Button, ModalHeader } from 'reactstrap';
import { ErrorToast } from './Toasts';

const PostForm = ({showForm,toggleForm,handleSavePost}) => {
    const[title,setTitle] = useState('');
    const[body,setBody] = useState('');

    const handleValidateAddPost = () =>{
        if(title!=='' && body!==''){
            handleSavePost(title,body);
        }else{
            ErrorToast("Please complete all details")
        }
        setTitle('');
        setBody('');
    }

 
    return (  
        <Modal isOpen={showForm} toggle={toggleForm}>
            <ModalHeader toggle={toggleForm}>
                Add Post
            </ModalHeader>
            <ModalBody className="p-5">
                <FormGroup>
                    <Input 
                        onChange={(e)=>setTitle(e.target.value)} 
                        placeholder="title" 
                    />
                </FormGroup>
                <FormGroup>
                    <Input 
                        onChange={(e)=>setBody(e.target.value)}
                        placeholder="body" 
                    />
                </FormGroup> 
                <FormGroup className="text-right">
                    <Button 
                        onClick={handleValidateAddPost} 
                        color="info"
                    >
                        ADD POST
                    </Button>

                </FormGroup>
            </ModalBody>
        </Modal>
    );
}
 
export default PostForm;