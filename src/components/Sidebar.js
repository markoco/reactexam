import React from 'react';

const Sidebar = () => {
    return ( 
        <>
            <div className="col-lg-2"></div> 
            <nav 
                className="col-lg-2" 
                style={{ 
                    height:"100vh",
                    borderRight:"1px solid #dae0e3",
                    zIndex:4, 
                    position:"fixed", 
                    top: 0
                }}
                >
                <div style={{height:"75px"}}></div>
                <ul className="mt-5 list-unstyled" >
                    <li><a>Home</a></li>
                    <li><a>Tables</a></li>
                    <li><a>Messages</a></li>
                </ul>
            </nav>
        </>
    );
}
 
export default Sidebar;