import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

//Bootstrap
import 'bootstrap/dist/css/bootstrap.css';
//Toastify
import 'react-toastify/dist/ReactToastify.css';
import {toast} from 'react-toastify';

toast.configure({
  autoClose:3000,
  draggable: false,
  hideProgressBar: true,
  newestOnTop: true,
  closeOnClick: true,
  zIndex: 10
})

ReactDOM.render(<App />, document.getElementById('root'));
